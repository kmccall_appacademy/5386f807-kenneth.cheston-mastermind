class Code
  attr_reader :pegs

  PEGS = { r: "red", g: "green", y: "yellow", b: "blue", p: "purple", o: "orange" }

  def initialize(pegs)
    @pegs = pegs
  end

  def self.peg?(input)
    array = input.downcase.split("")
    array.all? { |color| PEGS.key?(color.to_sym) }
  end

  def [](i)
    pegs[i]
  end

  def self.parse(pegs)
    raise "Invalid colors." if peg?(pegs) == false || pegs.size > 4
    pegs = pegs.downcase.chars
    Code.new(pegs)
  end

  def self.random
    random_array = []
    4.times { random_array << PEGS.keys.sample.to_s }
    secret_code = Code.new(random_array)
  end

  def exact_matches(code)
    counter = 0
    self.pegs.each_with_index { |color, index| counter += 1 if color == code.pegs[index] }
    counter
  end

  def near_matches(code)

    near_ma = []
    answer = self.pegs
    guess = code.pegs

    answer.each_with_index do |color, idx|
      guess.each_with_index do |color_2, idx_2|
        if color == color_2 && idx != idx_2 && !(near_ma.include?(idx_2)) && guess[idx_2] != answer[idx_2]
          near_ma << idx_2
          break
        end
      end
    end
    near_ma.size
  end

  def ==(input)
    if input.class == Code && self.class == Code
      self.pegs == input.pegs
    else false
    end
  end
end

class Game
  attr_reader :secret_code

  def initialize(code = Code.random)
    @secret_code = code
  end

  def play
    puts "Welcome to mastermind"
    i = 0
    while i < 10
      get_guess
      if won?(@guess)
        return puts "You won!"
      else display_matches(@guess)
      end
      i += 1
    end
    puts "You lose :("
  end

  def won?(guess)
    @secret_code == guess
  end

  def get_guess
    puts "Input Guess"
    begin
      guess = gets.chomp.strip
      @guess = Code.parse(guess)
    rescue
      puts "Invalid input, enter a valid combination:"
      retry
    end
  end

  def display_matches(code)
    puts "You have #{secret_code.near_matches(code)} near matches and #{secret_code.exact_matches(code)} exact matches."
  end
end


if __FILE__ == $PROGRAM_NAME
  Game.new.play
end
